<?php

if (! (isset($imgfilter) && $imgfilter))
        $imgfilter = 'brightness(0.5) sepia(1) hue-rotate(90deg) saturate(3) contrast(1.4) brightness(3) blur(0.2px)';
